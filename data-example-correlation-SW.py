#____coding: utf8___
__author__= 'Volkov Artem - artem.n@5glab.ru'
__program_name__ = 'Check-correlation-model'
__version__ = '2018.1.0'	


#    Exmaple correlation 
##   This program module for check multiparameter correlation analysis based on correlation dates 
###  Project "Earthquake prediction based on synergy of Space Weather analysis and IoT (biological objects)"
#### Python version 2.7

import csv 			       			#import the csv module for parsing functions
import sys        					#import the sys module for systems functions
import math  as m 					#import the math module for using basic mathematic for processing 
from numpy import var, std, mean, arange
import matplotlib.pyplot as plt; plt.rcdefaults() #impport th pyplot module for vizualization functions (including Graps)




def FileCSVProcessing():
	#This Function Read new File and on the next stage formated to Internal Format for next stage - processing
	filePath = raw_input('Enter name of file for next processing:' + '' )
	fileData = open(filePath, 'rb') #open csv file
	reader = csv.reader(fileData, quoting=csv.QUOTE_NONNUMERIC, delimiter=',', quotechar ='"')
	headers = reader.next() #skip a headers
	print(headers)
	A1 = []; A2 = []; A3 = []; A4 = []; A5 = []; # There are collumns of Matrix

	for row in reader:
		A1.append(row[0]);  # write data from the 1'st collumn "Tank temperature degrees Fahrenheit"
		A2.append(row[1]);  # write data from the 2'd collumn "Petrol temperature degrees Fahrenheit"
		A3.append(row[2]);  # write data from the 3'd collumn "Initial tank pressure pounds-square inch"
		A4.append(row[3]);  # write data from the 4'th collumn "Petrol pressure pounds-square inch"
		A5.append(row[4]);  # write data from the 5'd collumn "Hydrocarbons escaping grams"
	
	fileData.close() #clousing file

	return A1, A2, A3, A4, A5


def FilesCSVProcessingSW():
	#SW mean - Space Weather
	#This Function Read new File and on the next stage formated to Internal Format for next stage - processing
	pass


def MatrixTransform(InputMatrix):
	#This Functions relized the processing algorithm- matrix transformation from Imput matrix to Standart Type
	ArrayMatExp = []          # The array of mathematical expection after settlement
	ArrayMatDev = []          # The array of  mathematical deviation after settlement
	OutputMatrixStandart = [] # The output array which are transformated to Standart Type
	# The First Step - calculate mathematical expectation	
	if len(InputMatrix[0])!=0:
		# Check Matrix Size 
		for j in xrange(0, 5):
			MatExpNumerator = 0
			MatExp = mean(InputMatrix[j])
			ArrayMatExp.append(MatExp)
	else:
		# Write a algorithm, which will align matrix
		print "Some troubles with matrix, hasn't data"

	# The Second Step - calculate mathematical deviation
	if len(ArrayMatExp)!=0:
		# Check Matrix Size 
		for j in xrange(0, 5):
			MatVarNumerator = 0
			MatVar = std(InputMatrix[j])
			ArrayMatDev.append(MatVar)
	else:
		# Write a algorithm, which will align matrix
		print "Some troubles with matrix, hasn't mathematical expection" 

	# The third step is the assessment of the elements of the matrix and reduction to the standard form	
	OutputMatrixStandart = InputMatrix
	for j in xrange(0, 5):
		## u_ij - new elements of standart type matrix "U" (based on theory)
		u_ij = 0
		for i in xrange(len(InputMatrix[0])):
			u_ij = (InputMatrix[j][i] - ArrayMatExp[j])/ArrayMatDev[j]
			OutputMatrixStandart[j][i] = u_ij

	print 'The Matrix was transformed successfully...' 
	return OutputMatrixStandart



def CorrelationMomentFunction(InputMatrix):
	#This Functions relized the processing algorithm - multuparameter correlation analyze based on Standart matrix type and returned correlation Moment (KSI)
	ArrayMatExp = []          # The array of mathematical expection after settlement
	ArrayKSI = []             # The array of correlation Moment (KSI) between thirst (A) parameter and other parameters

	# The First Step - calculate mathematical expectation	
	if len(InputMatrix[0])!=0:
		# Check Matrix Size 
		for j in xrange(0, 5):
			MatExpNumerator = 0
			MatExp = mean(InputMatrix[j])
			ArrayMatExp.append(MatExp)
	else:
		# Write a algorithm, which will align matrix
		print "Some troubles with matrix, hasn't data"

	# The Second Step - calculate KSI 
	for j in xrange(0, 5):
		## ksi_ik - parameter KSI (correlation Moment) between (J:K)-> [1:2; 1:3; 1:4; 1:5; 4;5]
		ksi_ik = 0
		#print str(j) + "\n"
		for i in xrange(len(InputMatrix[0])):
			if j < 4 :
				ksi_ik += ((InputMatrix[0][i]-ArrayMatExp[0])*(InputMatrix[j+1][i]-ArrayMatExp[j+1]))
				#print str(ksi_ik)
			else: 
				ksi_ik += ((InputMatrix[3][i]-ArrayMatExp[3])*(InputMatrix[4][i]-ArrayMatExp[4]))

		ArrayKSI.append(ksi_ik/len(InputMatrix[0])) 

	return ArrayKSI


def CoeffCorrelationFunction(StandartMatrix):
	#This Function relized the processing algorithn=m - multiparameter correlation analyze based on Standart matrix type and returned correlation coefficient
	ArrayRU = [] # The array of correlation coefficient (RU) between thirst (A) parameter and other parameters

	for j in xrange(0, 5):
		## ru_ik - parameter KSI (correlation Moment) between (J:K)-> [1:2; 1:3; 1:4; 1:5; 4;5]
		ru_ik = 0
		for i in xrange(len(StandartMatrix[0])):
			if j < 4 :
				ru_ik += StandartMatrix[0][i]*StandartMatrix[j+1][i]
				#print str(ru_ik)
			else: 
				ru_ik += StandartMatrix[3][i]*StandartMatrix[4][i]

		ArrayRU.append(ru_ik/len(StandartMatrix[0])) 

	return ArrayRU

def DrawPlotFunctionCoeff(InputMatrix):
	#This function for draw Bar graph 
	print str(InputMatrix)
	objects = ('A / X_r', 'A / K_id', 'A / Pr_sl', 'A / m_b', 'Pr_sl / m_b')
	y_pos = arange(len(objects))
	performance = InputMatrix
 
	plt.bar(y_pos, performance, align='center', alpha=0.5)
	plt.xticks(y_pos, objects)
	plt.ylabel('Correlation Coeff.')
	plt.title('Correlation Coeff. between research parameters')
 
	plt.show()


def DrawScatterPlot(StandartMatrix):
	#This plot for draw scatter graph
	import numpy as np
	import matplotlib.pyplot as plt

	# Fixing random state for reproducibility
	#np.random.seed(19680801)
#	N = 50
	x = arange(0, len(StandartMatrix[0]), 1)
	A =     StandartMatrix[0]
	Xr =    StandartMatrix[1]
	K_id =  StandartMatrix[2]
	Pr_sl = StandartMatrix[3]
	m_b =   StandartMatrix[4]

	
	#plt.subplot(321)
	#plt.scatter(x, y, s=80, edgecolors='none', alpha=0.5)

	#plt.subplot(322)
	#plt.scatter(x, y, s=80, c='r', edgecolors='red', alpha=0.5)
	fig = plt.figure()
	plt.title("The Distributions")
	
	ax1 = fig.add_subplot(231)
	ax1.scatter(x, A, s=80, c='b', edgecolors='none', alpha=0.5, label='A')
	ax1.scatter(x, Xr, s=80, c='r', edgecolors='gray', alpha = 0.5, label='Xr')
	ax1.grid(True)
	plt.legend(loc='upper left')

	ax2 = fig.add_subplot(232) 
	ax2.scatter(x, A, s=80, c='b', edgecolors='none', alpha = 0.5, label='A')
	ax2.scatter(x, K_id, s=80, c='purple', edgecolors='gray', alpha = 0.5, label='K_id')
	ax2.grid(True)
	plt.legend(loc='upper left')

	ax3 = fig.add_subplot(233)
	ax3.scatter(x, A, s=80, c='b', edgecolors='none', alpha = 0.5, label = 'A')
	ax3.scatter(x, Pr_sl, s=80, c='g', edgecolors = 'gray', alpha = 0.5, label = 'Pr_sl')
	ax3.grid(True)
	plt.legend(loc='upper left')

	ax4 = fig.add_subplot(234)
	ax4.scatter(x, A, s=80, c='b', edgecolors='none', alpha = 0.5, label = 'A')
	ax4.scatter(x, m_b, s=80, c='y', edgecolors='gray', alpha=0.5, label = 'm_b')
	ax4.grid(True)
	plt.legend(loc='upper left')

	ax5 = fig.add_subplot(235)
	ax5.scatter(x, Pr_sl, s=80, c='b', edgecolors='none', alpha = 0.5, label = 'Pr_sl')
	ax5.scatter(x, m_b, s=80, c='black', edgecolors='gray', alpha=0.5, label='m_b')
	ax5.grid(True)
	plt.legend(loc='upper left')

	plt.show()

	


print '/////-----------------------------////////'

print str(CorrelationMomentFunction(FileCSVProcessing()))

print 'The correlation Moment was calculated successfuly...'
print '//////...........................////////'

DrawPlotFunctionCoeff(CoeffCorrelationFunction(MatrixTransform(FileCSVProcessing())))

print 'The Correletion coeff. was calculated successfuly... and builded graph'
print '/////............................///////'
DrawScatterPlot(MatrixTransform(FileCSVProcessing()))